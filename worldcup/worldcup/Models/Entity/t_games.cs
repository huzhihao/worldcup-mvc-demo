﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace worldcup.Models.Entity
{
    public class t_games
    {
        public int? id { get; set; }
        public int? countryAId { get; set; }
        public string countryAname { get; set; }
        public int? countryBId { get; set; }
        public string countryBname { get; set; }
        public DateTime? gamedate { get; set; }
        public string group { get; set; }
        public string score { get; set; }
        public string city { get; set; }

        public string status { get; set; }
        /// <summary>
        /// 比赛最后结果
        /// </summary>
        public int? winner { get; set; }
    }
}