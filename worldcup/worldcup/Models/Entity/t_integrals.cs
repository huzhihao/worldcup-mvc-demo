﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace worldcup.Models.Entity
{
    public class t_integrals
    {
        public int? id { get; set; }
        public int? score { get; set; }
        public string content { get; set; }
        public int? userid { get; set; }
        public DateTime? createAt { get; set; }

        public int? rank { get; set; }
    }
}