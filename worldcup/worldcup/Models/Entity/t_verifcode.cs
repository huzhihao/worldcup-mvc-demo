﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace worldcup.Models.Entity
{
    public class t_verifcode
    {
        public int? id { get; set; }
        public string mobile { get; set; }
        public string verifcode { get; set; }
        public DateTime? expiredate { get; set; }
        public string msg { get; set; }
        public DateTime? createtime { get; set; }
    }
}