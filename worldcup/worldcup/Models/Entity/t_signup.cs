﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace worldcup.Models.Entity
{
    public class t_signup
    {
        public int? id { get; set; }
        public string mobile { get; set; }
        public string ip { get; set; }
        public DateTime? createAt { get; set; }
    }
}