﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace worldcup.Models.Entity
{
    public class t_users
    {
        public int? id { get; set; }
        public string wxid { get; set; }
        public string mobile { get; set; }
        public string headimg { get; set; }
        public int? score { get; set; }
        public int? ranking { get; set; }
        public DateTime? creatat { get; set; }

        public string token { get; set; }

    }
}