﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace worldcup.Models.Entity
{
    public class t_jingcai
    {
        public int? id { get; set; }
        public int? gameid { get; set; }
        public int? winnerid { get; set; }
        public string winnerName { get; set; }
        public DateTime? createAt { get; set; }
        public int? userid { get; set; }
    }
}