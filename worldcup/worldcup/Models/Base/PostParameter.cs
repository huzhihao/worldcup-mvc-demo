﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace worldcup.Models
{
    /// <summary>
    /// 前端调用webapi方法的统一输入参数
    /// </summary>
    public class PostParameter<T>
    {
        /// <summary>
        /// 具体的参数实体
        /// </summary>
        [Required]
        public T parameter { get; set; }

    }
}
