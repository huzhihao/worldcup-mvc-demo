﻿using System;
using System.Collections.Generic;
using System.Text;

namespace worldcup.Models
{
    /// <summary>
    /// 返回的通用类型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PostResult<T>
    {
        // <summary>
        // 0=成功，1=失败
        // </summary>
        public int ret { get; set; }

        /// <summary>
        /// 返回消息
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// 返回消息代码
        /// </summary>
        //public string code { get; set; }

        /// <summary>
        /// 返回的数据，可以是Entity，也可以是List
        /// </summary>
        public T data { get; set; }

        /// <summary>
        /// 执行毫秒数
        /// </summary>
        //public long millis { get; set; }

        public PostResult<T> Error(string _msg)
        {
            //ret = 1;
            msg = _msg;
            return this;
        }
        public PostResult<T> Sucess(string _msg = "操作成功")
        {
            //ret = 1;
            msg = _msg;
            return this;
        }
        public PostResult<T> Sucess(T t, string _msg = "操作成功")
        {
            //ret = 1;
            msg = _msg;
            data = t;
            return this;
        }

    }

    /// <summary>
    /// 返回翻页类型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PostResultList<T>
    {
        /// <summary>
        /// 0=成功，1=失败
        /// </summary>
        //public int ret { get; set; }

        /// <summary>
        /// 返回消息
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// 返回消息代码
        /// </summary>
        //public string code { get; set; }


        /// <summary>
        /// 返回的数据，可以是Entity，也可以是List
        /// </summary>
        public T data { get; set; }

        /// <summary>
        /// 执行毫秒数
        /// </summary>
        //public long millis { get; set; }

        /// <summary>
        /// 返回记录总条数
        /// </summary>
        public int total { get; set; }

        /// <summary>
        /// 总页数，每页条数根据用户的输入，默认=20
        /// </summary>
        public int pagecount
        {
            get
            {
                var pageCount = total / pagesize + (total % pagesize == 0 ? 0 : 1);
                return pageCount == 0 ? 1 : pageCount;
            }
            set
            {
                var pageCount = total / pagesize + (total % pagesize == 0 ? 0 : 1);
                value = (pageCount == 0 ? 1 : pageCount);
            }
        }
        public int pagesize { get; set; } = 20;
        /// <summary>
        /// 当前页码，从1开始
        /// </summary>
        public int pageindex { get; set; } = 1;

        public PostResultList<T> Error(string _msg)
        {
            //ret = 1;
            msg = _msg;
            return this;
        }
        public PostResultList<T> Sucess(string _msg = "操作成功")
        {
            //ret = 1;
            msg = _msg;
            return this;
        }
        public PostResultList<T> Sucess(T t, string _msg = "操作成功")
        {
            //ret = 1;
            msg = _msg;
            data = t;
            return this;
        }

    }
}
