﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace worldcup.Models
{
    #region 查询

    //查询排行榜
    public class RankResponse
    {
        public string headimg { get; set; }
        public string mobile { get; set; }
        public string score { get; set; }
    }

    //查询竟猜记录
    public class JingcaiResonse
    {
        /// <summary>
        /// 比赛结果
        /// </summary>
        public int? winner { get; set; }

        /// <summary>
        /// 猜测结果
        /// </summary>
        public int? winnerid { get; set; }
        /// <summary>
        /// 猜测结果
        /// </summary>
        public string  winnername { get; set; }

        public string countryAname { get; set; }
        public string countryAFlag { get; set; }

        public string countryBname { get; set; }
        public string countryBFlag { get; set; }

        /// <summary>
        /// 比赛名称
        /// </summary>
        public string group { get; set; }
        /// <summary>
        /// 比赛日期
        /// </summary>
        public DateTime gamedate { get; set; }

    }

    //查询最新赛事
    public class NextGames
    {
        public int? id { get; set; }
        public int? countryAId { get; set; }
        public string countryAname { get; set; }
        public int? countryBId { get; set; }
        public string countryBname { get; set; }
        public DateTime? gamedate { get; set; }
        public string group { get; set; }
        public string score { get; set; }
        public string city { get; set; }

        public string status { get; set; }
        /// <summary>
        /// 比赛最后结果
        /// </summary>
        public int? winner { get; set; }
        public string countryAFlag { get; set; }
        public string countryBFlag { get; set; }
    }

    //登录，验证码
    public class Login
    {
        public string mobile { get; set; }
        public string verifycode { get; set; }
    }
    public class LoginResponse
    {
        public string token { get; set; }
    }
    public class SendMsg
    {
        public string mobile { get; set; }
    }

    //签到
    public class Sign
    {
        public string token { get; set; }
    }

    //提交竞猜
    public class Submit
    {
        /// <summary>
        /// 赛程id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 猜测结果1,2,3
        /// </summary>
        public int winnertype { get; set; }
        public string token { get; set; }
    }

    #endregion
}