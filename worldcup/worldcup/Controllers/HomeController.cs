﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using worldcup.DataAccess;

namespace worldcup.Controllers
{
    public class HomeController : Controller
    {
        DataService service = new DataService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Main(string token)
        {
            ViewBag.NextGames = service.NextGames();
            return View();
        }
        //[Route("Rank/{token}")]
        public ActionResult Rank(string token)
        {
            ViewBag.list = service.RankList();
            return View();
        }
        //[Route("JingChai/{token}/{type}")]
        public ActionResult JingChai(string token, string type)
        {
            ViewBag.type = type;
            ViewBag.token = token;
            ViewBag.list = service.JingcaiResult(token, type);

            return View();
        }

        public ActionResult ActRule()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
    }
}