﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using worldcup.DataAccess;
using worldcup.Models;

namespace worldcup.Controllers
{
    public class ExecController : Controller
    {
        DataService service = new DataService();

        #region 执行
        [HttpPost]
        public JsonResult SendMsg(SendMsg info)
        {
            var res = service.SendMsg(info);
            return Json(res);
        }

        [HttpPost]
        public JsonResult Login(Login info)
        {
            var res = service.Login(info);
            return Json(res);
        }

        [HttpPost]
        public JsonResult Sign(Sign info)
        {
            var res = service.Sign(info.token);
            return Json(res);
        }

        [HttpPost]
        public JsonResult Submit(Submit info)
        {
            var res = service.Submit(info);
            return Json(res);
        }

        #endregion



    }
}
