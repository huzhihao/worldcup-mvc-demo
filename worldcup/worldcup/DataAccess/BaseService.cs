﻿using MySqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace worldcup.DataAccess
{
    public class BaseService
    {
        private SqlSugarClient _sqlsugarClient;
        /// <summary>
        /// easycms库
        /// </summary>
        public SqlSugarClient db
        {
            get
            {
                if (_sqlsugarClient == null)
                {
                    var conn = "server=localhost;Database=db_football;Uid=root;Pwd=123";
                    var db = new SqlSugarClient(conn);
                    _sqlsugarClient = db;
                }
                return _sqlsugarClient;
            }
        }


    }
}